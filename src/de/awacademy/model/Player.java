package de.awacademy.model;

public class Player {


    // Eigenschaften - Variablen

    private int x;
    private int y;
    private int h;
    private int w;


    // Konstruktor

    public Player(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 100;
        this.w = 10;
    }


    // Methoden

    public void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;
    }


    // Getter

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }
}
