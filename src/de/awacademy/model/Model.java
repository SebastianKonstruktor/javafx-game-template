package de.awacademy.model;

import de.awacademy.Timer;

public class Model {


    // Eigenschaften - Variablen

    private Player player;
    private Ball ball;
    private Timer timer;
    public final double WIDTH = 1200;
    public final double HEIGHT = 800;
    int counter = 0;
    boolean treffer = false;
    long starttime;


    // Konstruktor

    public Model(Timer timer) {
        this.timer = timer;
        this.player = new Player(10, 10);
        this.ball = new Ball(10, 10);
        this.starttime = System.currentTimeMillis();
    }


    // Methoden


    public void collision() {
        if (ball.getX() >= player.getX() && ball.getX() <= player.getX() + player.getW() && ball.getY() >= player.getY() && ball.getY() <= player.getY() + player.getH())
            counter++;
        treffer = false;
        ball.move();

    }

    public void update(long nowNano) {
        getBall().move();
        collision();
        if (System.currentTimeMillis() - getStarttime() > 30000) {
            timer.stop();
        }
    }


    // Getter

    public Player getPlayer() {
        return player;
    }

    public long getStarttime() {
        return starttime;
    }

    public Ball getBall() {
        return ball;
    }

    public int getCounter() {
        return counter;
    }
}







