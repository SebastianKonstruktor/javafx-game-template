package de.awacademy.model;

public class Ball {


    // Eigenschaften - Variablen

    private int x;
    private int y;
    private int d;
    private int dx;
    private int dy;


    // Konstruktor

    public Ball(int x, int y) {
        this.x = 600;
        this.y = 400;
        this.d = 30;
        this.dx = 3;
        this.dy = 3;
    }


    // Methoden

    public void move() {
        x += dx;
        y += dy;
        if (x <= 0 || x >= 1200 - d) {
            dx *= -1;

        } else if (y <= 0 || y >= 800 - d) {
            dy *= -1;
        }

    }


    // Getter

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getD() {
        return d;
    }
}

