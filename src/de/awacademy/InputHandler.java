package de.awacademy;

import de.awacademy.model.Model;
import javafx.scene.input.KeyCode;

public class InputHandler {


    // Eigenschaften - Variablen

    private Model model;


    // Konstruktor

    public InputHandler(Model model) {
        this.model = model;
    }


    // Methoden

    public void onKeyPressed(KeyCode keycode) {

        if (keycode == KeyCode.NUMPAD8) {
            model.getPlayer().move(0, -30);
        }
        if (keycode == KeyCode.NUMPAD2) {
            model.getPlayer().move(0, 30);
        }

    }
}
