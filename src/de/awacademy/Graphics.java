package de.awacademy;

import de.awacademy.model.Model;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class Graphics {


    // Eigenschaften - Variablen

    private GraphicsContext gc;
    private Model model;


    // Konstruktor

    public Graphics(GraphicsContext gc, Model model) {
        this.gc = gc;
        this.model = model;
    }

    // Methoden

    public void draw() {
        gc.clearRect(0, 0, model.WIDTH, model.HEIGHT);

        // Spielerbalken

        gc.setFill(Color.BLACK);
        gc.fillRect(model.getPlayer().getX(), model.getPlayer().getY(),
                model.getPlayer().getW(), model.getPlayer().getH());

        // Ball

        gc.setFill(Color.BLACK);
        gc.fillOval(model.getBall().getX(), model.getBall().getY(), model.getBall().getD(), model.getBall().getD());

        // Scorezähler

        gc.setFill(Color.BLACK);
        gc.setFont(new Font("", 20));
        gc.fillText("Score " + model.getCounter(), 600, 20);

    }
}
