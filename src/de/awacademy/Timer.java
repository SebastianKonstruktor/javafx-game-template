package de.awacademy;

import de.awacademy.model.Model;
import javafx.animation.AnimationTimer;

public class Timer extends AnimationTimer {


    // Eigenschaften - Variablen

    private Graphics graphics;
    private Model model;


    // Konstruktor

    public Timer(Graphics graphics, Model model) {
        this.graphics = graphics;
        this.model = model;
    }


    // Methoden

    @Override
    public void handle(long nowNano) {
        model.update(nowNano);
        graphics.draw();
    }
}
